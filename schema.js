const axios = require('axios');
const { GraphQLObjectType,
				GraphQLInt,
				GraphQLSchema,
				GraphQLList,
				GraphQLNonNull,
				GraphQLString,
				GraphQLScalarType,
			 } = require('graphql');


/*const customer = [
				{"id":"1","name":"James Bond","age":6,"email":"Bond@email.com"},
				{"id":"2","name":"James Smith","age":7,"email":"James@email.com"}
] ; */

const CustomerType = new GraphQLObjectType({
					name:'Customer',
					fields:() => ({
						id:{type:GraphQLString},
						name:{type:GraphQLString},
						email:{type:GraphQLString},
						age:{type:GraphQLInt}
					})
});


			 // RootQuery
const RootQuery = new GraphQLObjectType({
					name:'RootQueryType',
					fields:{
						customer:{
							type:CustomerType, // Return types , And It is a Object . 
							args:{             //  Arguments Passed By
								id:{type:GraphQLString}
							},
							resolve(parentValue,args){
								console.log(args); 
								const resById = axios.get('http://localhost:3000/customers/' + args.id)
									.then(res => res.data);
								return resById;
								/*
								 for(let i=0;i <= customer.length;i++){
									if(customer[i].age == args.age ){
											//console.log(args.id);
											//console.log(customer) 
											return customer[i] ;  
 									}
								} */
							}
						},
						customers:{
							type: new GraphQLList(CustomerType),
							resolve(parentValue,args){
								const resByall = axios.get('http://localhost:3000/customers')
									.then(res => res.data);
								return resByall;
							}
						},
						email:{
							type:CustomerType,
							args:{
								email:{type:GraphQLString}
							},
							resolve(parentValue,args){
								for(let i=0;i <= customer.length;i++){
									if(customer[i].email == args.email ){
											//console.log(args.email);
											console.log(customer[i].email) 
											return customer[i] ;  
									    //return axios.get('http://localhos:3000'+args.)	
									}
								}
							}
						}
					}
})


const mutation = new GraphQLObjectType({
			name:'Mutatation',
			fields:{
				addCustomer:{
					type:CustomerType,
					args:{
						name:{type:new GraphQLNonNull(GraphQLString)},
						email:{type:new GraphQLNonNull(GraphQLString)},
						age:{type:new GraphQLNonNull(GraphQLInt)}
					},
					resolve(parentValue,args){
						return axios.post("http://localhost:3000/customers/",{
							name:args.name,
							email:args.email,
							age:args.age
						})
						.then(res => res.data);
					}
				},
				editCustomer:{
					type:CustomerType,
					args:{
						id:{type:new GraphQLNonNull(GraphQLString)},
						name:{type:new GraphQLNonNull(GraphQLString)},
						email:{type:new GraphQLNonNull(GraphQLString)},
						age:{type:new GraphQLNonNull(GraphQLInt)}
					},
					resolve(parentValue,args){
						return axios.patch("http://localhost:3000/customers/"+args.id,{
							id:args.id,
							name:args.name,
							email:args.email,
							age:args.age
						})
						.then(res => res.data);
					}
				},
				deleteCustomer:{
					type:CustomerType,
					args:{
						id:{type:new GraphQLNonNull(GraphQLString)}
					},
					resolve(parentValue,args){
						return axios.delete("http://localhost:3000/customers/"+args.id,{
							id:args.id
						})
						.then(res => res.data);
					}
				}
			}
});

module.exports = new GraphQLSchema({
						query: RootQuery, 
						mutation:mutation
});